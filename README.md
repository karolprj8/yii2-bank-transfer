BankTransfer extension for the Yii2
===========

BankTransfer payment extension for the Yii2.

Installation
====

Add to the composer.json file following section:

```
php composer.phar require --prefer-dist karolprj8/yii2-bank-transfer "*"
```

```
"karolprj8/yii2-bank-transfer": "dev-master"
```

Add to to you Yii2 config file this part with component settings:

```php

```
