<?php
/**
 * File BankTransfer.php.
 *
 * 
 */

namespace karolprj8;

use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\base\Component;


use twofox\goods\models\Goods;
use yii\helpers\Url;
use frontend\modules\payments\models\Payments;
use backend\modules\ticket\models\Vouchers;


class BankTransfer extends Component {
    
    const CLASSNAME = 'BankTransfer';

    //region Log levels
    /*
     * Logging level can be one of FINE, INFO, WARN or ERROR.
     * Logging is most verbose in the 'FINE' level and decreases as you proceed towards ERROR.
     */
    const LOG_LEVEL_FINE = 'FINE';
    const LOG_LEVEL_INFO = 'INFO';
    const LOG_LEVEL_WARN = 'WARN';
    const LOG_LEVEL_ERROR = 'ERROR';
    //endregion

    //region API settings
    public $accountNo;
    public $company;
    public $address;
    public $bank;
    public $currency = 'PLN';
    

    public function createPayments($data = [], $fastorderemail=null) {
        //print_r($data);

        $total = 0;
        $st = 0;

        $items = [];
        $products_count = [];
        foreach ($data as $value) {

            if (!$goods = Goods::findOne($value['id']))
                continue;
            
            if($st==0){
                $pay_id = $goods -> local -> payments["przelew"];
                $this->accountNo = Yii::$app->params['payments']["przelew"]['accounts'][$pay_id]['konto'];
                $this->company = Yii::$app->params['payments']["przelew"]['accounts'][$pay_id]['firma'];
                $this->address = Yii::$app->params['payments']["przelew"]['accounts'][$pay_id]['adres'];
				$this->bank = Yii::$app->params['payments']["przelew"]['accounts'][$pay_id]['bank'];                
            }
            
            $items['goods'][] = $goods;
            $items['products'] = array();
            
            $count = max(1, intval(@$value['count']));
            
            $total += $goods -> getPrice() * $count;
			$good_id = str_replace('-', '', substr($goods->local->slug, 0 ,5)); 
            if (isset($value['products'])) {
                $Products = $goods -> getProd();
                foreach ($value['products'] as $product) {
                    if (!isset($Products[$product['id']]))
                        continue;
                    
                    $count = max(1, intval(@$product['count']));
                    $product = $Products[$product['id']];                    
                    $items['products'][] = $product;                    
                    $total += $product -> getPrice() * $count;
                    $products_count[$product['id']] = !isset($products_count[$product['id']]) ? $count : $products_count[$product['id']] +$count;
                }
            }
            
            $st++;
        }


		
		$inpayment = new Payments();
        $inpayment -> payment_id = 'BT-'.time();
        $inpayment -> gateway = self::CLASSNAME;
        $inpayment -> gateway_name = Yii::$app->params['payments']["przelew"]['accounts'][$pay_id]['label'];
            
        if(\Yii::$app->user->isGuest){
            $inpayment -> email = $fastorderemail;
            $email = $fastorderemail;
        }else{
            $inpayment -> user_id = Yii::$app -> user -> id;
            $email = Yii::$app -> user -> identity -> email;
        }
            
        $inpayment -> amount = $total;
        $inpayment -> currency = $goods -> currency;
        $inpayment -> cart = $data;
		// 		
		
        if ($inpayment -> save()){
            
			$inpayment -> cart = $data;
			$inpayment_id = str_replace(array('1', '0', '8', '2'), array('W', 'R', 'Y', 'X'), $inpayment -> id);
            $inpayment -> payment_id  = strtoupper('BT-'.$inpayment_id.'-'.$good_id);
			$inpayment -> save();            
            
            Yii::$app->mailer->compose(['html' => 'przelew'], ['items'=>$items, 'email'=>$email, 'accountNo'=>$this->accountNo, 'company'=>$this->company, 'address'=>$this->address, 'bank'=>$this->bank, 'inpayment'=>$inpayment, 'payment_id'=>$inpayment -> payment_id])        
            ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->config->get('CONTACT.ORGANIZATION_NAME')])
            ->setTo($email)
            ->setSubject(Yii::$app->config->get('CONTACT.ORGANIZATION_NAME') . ' :: ' . Yii::t('email', 'Dane do przelewu'))
            ->send();	
            		
			$inpayment -> cart = json_decode($inpayment -> cart, 1);			
			return [0=>'przelew', 1=>\Yii::$app->view->render('//site/payments/default/przelew', ['items'=>$items, 'products_count'=>$products_count, 'email'=>$email, 'accountNo'=>$this->accountNo, 'company'=>$this->company, 'address'=>$this->address, 'bank'=>$this->bank, 'inpayment'=>$inpayment, 'payment_id'=>$inpayment -> payment_id])];
		}
        			
        exit ;
    }

}
